﻿using static System.Console;
namespace Hello
{
    class Program
    {
        /// <summary>
        /// main
        /// </summary>
        /// <param name="args">parameters</param>
        static void Main(string[] args)
        {
            Write("Hello ");
            Write("World");
            Write("!!!");
            ReadKey();
        }
    }
}
